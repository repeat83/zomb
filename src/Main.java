import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Main {

    public static int WIDTH = 800;
    public static int HEIGHT = 800;
    public static final int COUNT_CREATURES = 3;

    public static population population;

    public static void main(String[] args) throws InterruptedException {

        //if (true) return;
        new Canvas();
        population = new population();
        population.create(COUNT_CREATURES);


//////////////////////////
        ArrayList<double[]> I = new ArrayList<>();
        ArrayList<double[]> Ans = new ArrayList<>();

        for (int i = 0; i<15; ++i) {
            I.add(new double[]{-1*Math.random()*10, Math.random() * 800, Math.PI});
            Ans.add(new double[] {0,0});
            I.add(new double[]{800+Math.random()*10, Math.random() * 800, 0});
            Ans.add(new double[] {0.5,0});

            I.add(new double[]{Math.random() * 800, -1*Math.random()*10, Math.PI/2});
            Ans.add(new double[] {0,0});
            //I.add(new double[]{Math.random() * 800, 700+Math.random()*10, 3/2*Math.PI});
            //Ans.add(new double[] {0.25,0});
        }
        for (int i = 0; i<I.size(); ++i){

            //reflection
            double x = I.get(i)[0];
            double y = I.get(i)[1];
            double heading = I.get(i)[2];

            if ((x <=0) | (x>=(Main.WIDTH-0))) {
                if (heading > 0 && heading < Math.PI) {
                    heading = Math.PI-heading;
                }
                else {
                    heading = 3*Math.PI-heading;
                }
            }
            if ((y <=0) | (y>=(Main.HEIGHT-0))) {
                heading = 2*Math.PI-heading;
            }
            //Ans.add(new double[] {heading/(2*Math.PI),0});
        }

        trainer trainer = new trainer();
for (int z = 0; z<population.creatures.size(); ++z) {
    NET NET = population.creatures.get(z).brain;

    double start = 400;
    int x, y;
    double iteration = start;
    double step = 1;

    // обучение online - каждый тренировочный "пресет" прогоняется 2-3 раза и выполняется переход к следующему "пресету"
    do {
        for (int tr_data = 0; tr_data < I.size(); ++tr_data) {
            x = (int) (Main.WIDTH - Main.WIDTH * iteration / start);

            trainer.training(NET, I.get(tr_data), Ans.get(tr_data), step);
            trainer.training(NET, I.get(tr_data), Ans.get(tr_data), step);
            trainer.training(NET, I.get(tr_data), Ans.get(tr_data), step);
            y = 0;
            for (int i = 0; i < NET.getNumNeyrons(NET.Layers.size() - 1); ++i) {
                y += (int) (NET.getNeyron(NET.Layers.size() - 1, i).local_gradient * 200);
            }
            Canvas.Render.addPoint(new Point2D.Double(x, y + 200), new Color(255, 0, 255));
        }
        population.creatures.get(z).updateGEN();
        Canvas.Render.reDraw();
        --iteration;
    } while (iteration > 0);
////////////////////////////////
    population.creatures.get(z).heading = Math.random()*2*Math.PI;
}


ArrayList<Double> tmp = new ArrayList<>();
        tmp.add(1.0);
        tmp.add(2.0);
        tmp.add(3.0);
        tmp.add(4.0);
        tmp.add(5.0);
        tmp.add(4.0);
        tmp.add(3.0);
        tmp.add(2.0);
//population.creatures.get(1).GENtoWeights(tmp);
population.creatures.get(1).updateGEN();

        do {
           Thread.sleep(5);
           Canvas.Render.reDraw();
           for (int id=0; id<population.creatures.size(); ++id) {
               zomb creature = population.creatures.get(id);
               creature.move();
           }

       } while (true);
    }
}

