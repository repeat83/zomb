import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Canvas extends JPanel {
    BorderLayout borderLayout1 = new BorderLayout();
    private ArrayList<Point2D.Double> points = new ArrayList<Point2D.Double>() ;
    private ArrayList<Color> colors = new ArrayList<Color>();

    public static Canvas Render;

    public Canvas() {
        //points = new ArrayList<Point2D.Double>();
        //colors = new ArrayList<Color>();
        try {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        Render = this;
        //Canvas c = new Canvas();
        JFrame frame = new JFrame();
        //frame.setBounds(100, 100, 192, 108);
        frame.getContentPane().setPreferredSize(new Dimension(Main.WIDTH, Main.HEIGHT));
        frame.setTitle("Pointsssss");
        frame.setSize(Main.WIDTH, Main.HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setUndecorated(true); //hide title
        //frame.add(c); // Add canvas data to jframe
        frame.add(this); // Add canvas data to jframe
        frame.setVisible(true);

        //change window data to actual workspace
        Main.WIDTH = frame.getContentPane().getWidth();
        Main.HEIGHT = frame.getContentPane().getHeight();
        System.out.println(Main.WIDTH);
        System.out.println(Main.HEIGHT);
    }

    public void addPoint(Point2D.Double point, Color color) {
        points.add(point);
        colors.add(color);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        double x,y,dir;
        for(int i = 0; i < points.size(); ++i) {
            if (i<Main.population.creatures.size()) { // отрисовываем животных, они у нас идут первыми
                x = points.get(i).getX();
                y = points.get(i).getY();
                dir = Main.population.creatures.get(i).heading;
                g2d.setColor(colors.get(i));
                //g2d.fillRect((int) points.get(i).getX(), (int) points.get(i).getY(), 5, 5);
                g2d.fillOval((int) x, (int)y, 10, 10);
                g2d.draw(new Line2D.Double(x + 5, y + 5, x + 5 + Math.cos(dir) * 10, y + 5 + Math.sin(dir) * 10));
                g2d.drawString(""+i,(int)x,(int)y);

            } else {
                g2d.setColor(colors.get(i));
                g.fillRect((int)points.get(i).getX(), (int)points.get(i).getY(), 2, 2);
            }

        }

        for (int z = 0; z<Main.population.creatures.size(); ++z) {
            zomb zomb = Main.population.creatures.get(z);
            double offset = Main.WIDTH - Main.population.creatures.size()*10;
            int c;
            int size = Main.population.creatures.get(z).gen.size();
            double t;
            for (int i=0; i<size; ++i){
                c = (int) (zomb.gen.get(i) / zomb.maxChromosome * 127);
                //System.out.println(z +"-" + i + " " + Main.population.creatures.get(z).maxChromosome + " / ");
                c = c & 0xff;
                g2d.setColor(new Color(c, c, 0));
                g2d.fill3DRect((int)offset + z * 10, i * 10, 10, 10, false);
            }
        }

    }

    public void reDraw() {
        repaint();
    }

    void jbInit() throws Exception {
        this.setLayout(borderLayout1);
    }
}
