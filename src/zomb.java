import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;

// packet neyron including as Modules - Dependencies

public class zomb {

    public int id;
    public NET brain = new NET();
    public Point2D.Double pos = new Point2D.Double();
    public double heading;
    public Point2D.Double vector_move = new Point2D.Double();
    public double fov;
    public double look_distance;
    public double nose_distance;
    public double speed = 0.3;//0.1+Math.random()*0.9;
    public int follow;
    public ArrayList<zomb> isee = new ArrayList<>();
    public Color color = new Color(0,(int)(this.speed*255),(int)(this.speed*255));
    public ArrayList<zomb> like = new ArrayList<>();

    public ArrayList<Double> gen = new ArrayList<Double>();
    public double maxChromosome = 0;
    public long lifeTime = System.currentTimeMillis(); // age

    zomb(double x, double y, double heading) {
        this.pos.setLocation(x,y);
        this.heading = heading;

        this.brain.createLayer(3); // x,y,heading
        this.brain.createLayer(5);
        this.brain.createLayer(2); // heading, speed
        updateGEN();

/*
        this.brain.getNeyron(1,0).setWeight(1.026744356731043);//        l1 w_0=1.026744356731043
        this.brain.getNeyron(1,0).setWeight(0, 0.9071640315976818);//        l1 w00=0.9071640315976818
        this.brain.getNeyron(1,0).setWeight(1, -0.27208789545669465);//        l1 w10=-0.27208789545669465
        this.brain.getNeyron(1,0).setWeight(2, 0.812827902808073);//        l1 w20=0.812827902808073
        this.brain.getNeyron(1,1).setWeight(-0.12165920561464993);//        l1 w_1=-0.12165920561464993
        this.brain.getNeyron(1,1).setWeight(0, 1.4486322694900746);//        l1 w01=1.4486322694900746
        this.brain.getNeyron(1,1).setWeight(1, -0.6711955787434551);//        l1 w11=-0.6711955787434551
        this.brain.getNeyron(1,1).setWeight(2, 1.1306763144857306);//        l1 w21=1.1306763144857306
        this.brain.getNeyron(2,0).setWeight(-4.034705716162193);//        l2 w_0=-4.034705716162193
        this.brain.getNeyron(2,0).setWeight(0, 3.0841764008094934);//        l2 w00=3.0841764008094934
        this.brain.getNeyron(2,0).setWeight(1, 0.9498030554360255);//        l2 w10=0.9498030554360255
        this.brain.getNeyron(2,1).setWeight(4.283396482128499);//        l2 w_1=4.283396482128499
        this.brain.getNeyron(2,1).setWeight(0, 1.251884673528399);//        l2 w01=1.251884673528399
        this.brain.getNeyron(2,1).setWeight(1, -0.1322812449420429);//        l2 w11=-0.1322812449420429
*/
    }

    zomb(int x, int y) {
        this(
                x,
                y,
                Math.random() * 2 * Math.PI
        );
    }

    zomb() {
        this(
                Math.random() * Main.WIDTH, // x
                Math.random()*Main.HEIGHT,  // y
                Math.random()*2*Math.PI     // heading
        );
    }

    public Point2D.Double getPosition(){
        return this.pos;
    }

    public Color getColor(){
        return this.color;
    }

    public void move() {
//        this.brain.setInput(new double[]{this.pos.x / Main.WIDTH, this.pos.y / Main.HEIGHT, this.heading / (Math.PI*2)});
        this.brain.setInput(new double[]{this.pos.x, this.pos.y, this.heading});
        this.brain.getOut();
//System.out.println(this.pos.y + " " + this.brain.getNeyron(1,0).getWeight(0) + " " + this.brain.getNeyron(2, 0).Fe*360);
        this.heading = this.brain.getNeyron(2, 0).Fe* 2*Math.PI;
        this.pos.x += Math.cos(this.heading)*this.speed;
        this.pos.y += Math.sin(this.heading)*this.speed;

        //reflection
        /*
        if ((this.pos.x <=0) | (this.pos.x>=(Main.WIDTH-0))) {
            if (this.heading > 0 && this.heading < Math.PI) {
                this.heading = Math.PI-this.heading;
            }
            else {
                this.heading = 3*Math.PI-this.heading;
            }
        }
        if ((this.pos.y <=0) | (this.pos.y>=(Main.HEIGHT-0))) {
            this.heading = 2*Math.PI-this.heading;
        }
        */
    }

    public void updateGEN() {
        this.gen.clear();
        for (int l = 1; l<brain.Layers.size(); ++l) {
            int NumNeyrons = brain.getNumNeyrons(l);
            int NumWeights = brain.getNumNeyrons(l-1);
            for (int n = 0; n<NumNeyrons; ++n) {
                this.gen.add(brain.getNeyron(l,n).w0);
                for (int w = 0; w<NumWeights; ++w) {
                    this.gen.add(brain.getNeyron(l,n).getWeight(w));
                    if (brain.getNeyron(l,n).getWeight(w) > maxChromosome)
                        maxChromosome = brain.getNeyron(l,n).getWeight(w);
                }
            }
        }
    }

    public void GENtoWeights(ArrayList<Double> gen){
        this.gen = gen;
        for (int l = 1; l<brain.Layers.size(); ++l) {
            int NumNeyrons = brain.getNumNeyrons(l);
            int NumWeights = brain.getNumNeyrons(l-1);
            for (int n = 0; n<NumNeyrons; ++n) {
                brain.getNeyron(l,n).setWeight(gen.get(0)); //set w0
                gen.remove(0); //remove used gen
                for (int w = 0; w<NumWeights; ++w) {
                    brain.getNeyron(l,n).setWeight(w,gen.get(0));
                    if (gen.get(0)> maxChromosome)
                        maxChromosome = gen.get(0);
                    gen.remove(0); //remove used gen
                }
            }
        }
//TODO проверить совпадает ли входной ген с гено сгенерированный из весов полученных из входного гена

    }
}
