import java.util.ArrayList;

/**
 * Created by repeat on 9/16/15.
 */
public class population {

    public ArrayList<zomb> creatures = new ArrayList<>();

    public population(){
    }

    public void create(int count){
        for (int id=0; id<count; ++id) {
            creatures.add(new zomb(Main.WIDTH/2,Main.HEIGHT/2));
            Canvas.Render.addPoint(creatures.get(id).getPosition(), creatures.get(id).getColor());
        }
    }

}
